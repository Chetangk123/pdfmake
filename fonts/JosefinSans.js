module.exports = {
	JosefinSans: {
		normal: __dirname + '/JosefinSans/JosefinSans-Regular.ttf',
		bold: __dirname + '/JosefinSans/JosefinSans-Medium.ttf',
		italics: __dirname + '/JosefinSans/JosefinSans-Italic.ttf',
		bolditalics: __dirname + '/JosefinSans/JosefinSans-MediumItalic.ttf'
	}
};
